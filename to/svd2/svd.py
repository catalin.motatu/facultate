import tkinter as tk
from tkinter import filedialog
from tkinter.filedialog import askopenfile
from PIL import Image
import cv2
import matplotlib.pyplot as plt
import numpy as np

my_w = tk.Tk()
my_w.geometry("400x300")
my_w.title('Proiect final TO - SVD 2')
my_font1=('times', 18, 'bold')
l1 = tk.Label(my_w,text='Adauga imagine pentru SVD', width=30, font=my_font1)  
l1.grid(row=1,column=1)
b1 = tk.Button(my_w, text='Alege imagine', 
   width=20,command = lambda:upload_file())
b1.grid(row=2,column=1) 

def draw_svd(A,U, S, VT, our_map):
  plt.subplot(221 )
  plt.title('Matricea originala')
  plt.imshow(A, cmap =our_map)
  plt.axis('off')
  plt.subplot(222)
  plt.title('Matricea U')
  plt.imshow(U, cmap =our_map)
  plt.axis('off')
  plt.subplot(223)
  plt.title('Matricea Sigma')
  plt.imshow(S, cmap =our_map)
  plt.axis('off')
  plt.subplot(224)
  plt.title('Matricea V')
  plt.imshow(VT, cmap =our_map)
  plt.axis('off')
  plt.show()

def upload_file():
    global img, my_w
    f_types = [('Jpg Files', '*.jpg')]
    filename = filedialog.askopenfilename(filetypes=f_types)
    img=Image.open(filename)

    img = cv2.iimg = cv2.imread(filename, 1)
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    plt.axis('off')
    plt.show()

    (b, g, r) = cv2.split(img)
    img_s = np.vstack([b, g, r])
    plt.imshow(img_s, cmap = 'gray')
    plt.axis('off')

    U, S, VT = np.linalg.svd(img_s)
    S = np.diag(S)
    draw_svd(img_s, U, S, VT, 'gray') 

my_w.mainloop()