## Despre

Scriptul ***svd.py*** ofera o interfata grafica pentru descompunerea SVD pentru imagini incarcate de pe disc.
Se poate utiliza butonul ***Alege imagine*** pentru a fi descompusa.
Dupa alegerea imaginii, se va afisa imaginea si descompunerea acesteia.

![](./view.png "")
--------------------------
![](./view_1.png "")

## Dependinte
Pentru rularea scriptului sunt necesare urmatoarele librarii: 
* matplotlib
* tkinter
* numpy
* pillow
* cv2

## Rulare aplicatie
```ssh
python svd.py
```