## Despre

Scriptul ***svd.py*** ofera o interfata grafica pentru descompunerea SVD.
Se poate utiliza capul de text pentru definirea matrice initiale.
Descompunerea se realizeaza la apasarea butonului ***Calculeaza SVD***
Dupa descompunerea se poate utiliza butonul ***Vizualizare grafica*** pentru a afisa o plotare a rezultatului.

![](./view.png "")

## Dependinte
Pentru rularea scriptului sunt necesare urmatoarele librarii: 
* matplotlib
* tkinter
* numpy

## Rulare aplicatie
```ssh
python svd.py
```