import numpy as np
import tkinter as tk
import matplotlib.pyplot as plt

window = tk.Tk()
A = []
U = []
s = []
VT = []

m = tk.Text(height=5, width=20)
m.insert('end-1c', "1 1 1\n2 2 2\n3 3 3")
window.title("Proiect final TO - SVD 1")
window.configure(width=500, height=300)
window.configure(bg='lightgray')

r_U = tk.Text(height=10, width=40)
r_S = tk.Text(height=10, width=40)
r_VT = tk.Text(height=10, width=40)

def rezolvaSvd():
    global A, U, s, VT
    rez = m.get('1.0', 'end-1c').split("\n")    
    for i in rez :
        A.append([float(n) for n in i.split(" ")])
    print (A)

    U, s, VT = np.linalg.svd(A)
    s = np.diag(s)

    r_U.delete(1.0, "end-1c")
    r_U.insert("end-1c", U)

    r_S.delete(1.0, "end-1c")
    r_S.insert("end-1c", s)

    r_VT.delete(1.0, "end-1c")
    r_VT.insert("end-1c", VT)

def draw_svd(A,U, S, VT, our_map="hot"):
  plt.subplot(221 )
  plt.title('Matrice originala')
  plt.imshow(A, cmap =our_map)
  plt.axis('off')
  plt.subplot(222)
  plt.title('Matricea U')
  plt.imshow(U, cmap =our_map)
  plt.axis('off')
  plt.subplot(223)
  plt.title('Matricea Sigma')
  plt.imshow(S, cmap =our_map)
  plt.axis('off')
  plt.subplot(224)
  plt.title('Matricea V')
  plt.imshow(VT, cmap =our_map)
  plt.axis('off')
  plt.show()

def truncate_u_v(S, U, VT):
  threshold = 0.001
  s = np.diag(S)
  index = s < threshold

  U[:,index] = 0
  VT[index,:]=0
  return U, VT

def afiseazaGrafic() :
    global A, U, s, VT
    print(A)
    print(U)
    print(s)
    print(VT)
    U, VT = truncate_u_v(s, U, VT)
    draw_svd(A, U, s, VT)

l = tk.Label(window, text = "Matrice initiala")
b1 = tk.Button(window, text = "Calculeaza SVD", command=rezolvaSvd)
b2 = tk.Button(window, text = "Vizualizare grafica", command=afiseazaGrafic)
rU = tk.Label(window, text = "Rezultat U")
rS = tk.Label(window, text = "Rezultat Sigma")
rVT = tk.Label(window, text = "Rezultat VT")
l.pack(pady=20, padx=10)
m.pack(padx=10)
b1.pack(padx=20, pady=5)
rU.pack(padx=20, pady=5)
r_U.pack(padx=20, pady=5)
rS.pack(padx=20, pady=5)
r_S.pack(padx=20, pady=5)
rVT.pack(padx=20, pady=5)
r_VT.pack(padx=20, pady=5)
b2.pack(padx=20, pady=5)

window.mainloop()