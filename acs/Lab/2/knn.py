import numpy as np
from functii import *
import time
import statistics as st


# Definire valori constante.
cale = "./faces/"
nrPersoane = 40
nrPozeAntrenare = 8
rezPozaX = 112
rezPozaY = 92
rezTotalaPoza = rezPozaX * rezPozaY
nrTotalPoze = nrPersoane * nrPozeAntrenare
A = np.zeros((nrTotalPoze, rezTotalaPoza))

# Matricea de antrenare
A = train(cale, nrPersoane, nrPozeAntrenare, rezTotalaPoza)

verificare_poza(A, 1, 1, nrPozeAntrenare, rezPozaX, rezPozaY)

nrPersoanaCautata = 21
pozaCautata = poza_de_test(cale, nrPersoanaCautata, 9, rezPozaX, rezPozaY)

nrPersoanaGasitaNN = test_nn(A, nrTotalPoze, pozaCautata, nrPozeAntrenare, rezPozaX, rezPozaY)

if (nrPersoanaCautata == nrPersoanaGasitaNN):
  print("Rezultatul NN este OK")
else :
  print("Rezultatul NN este NOK")

#1, 2, infinit, cos
norma = 'cos'
#3, 5, 7 sau 9
k = 9
nrPersoanaGasitaKNN = test_knn(k, norma, A, nrTotalPoze, pozaCautata, nrPozeAntrenare, rezPozaX, rezPozaY, rezPozaY)

if (nrPersoanaCautata == nrPersoanaGasitaKNN):
  print("Rezultatul kNN este OK")
else :
  print("Rezultatul kNN este NOK")

# Statistici.
nrTotalTeste = nrPersoane * (10 - nrPozeAntrenare)
nrRecunoasteriCorecte = 0
timpiInterogare = []

for i in range(1, (nrPersoane + 1)):
  for j in range(nrPozeAntrenare + 1, 11) :
    print("Citim poza %s, persoana %s" % (j, i))
    pozaCautata = poza_de_test(cale, i, j, rezPozaX, rezPozaY, 0)
    t0 = time.perf_counter()
    nrPersoanaGasitaNN = test_nn(A, nrTotalPoze, pozaCautata, nrPozeAntrenare, rezPozaX, rezPozaY, 0)
    t1 = time.perf_counter()
    t = t1 - t0
    timpiInterogare.append(t)
    if (nrPersoanaGasitaNN == i) :
      nrRecunoasteriCorecte += 1      

rataDeRecunoastere = nrRecunoasteriCorecte / nrTotalTeste
print('Rata de recunoastere: %f' % rataDeRecunoastere)

timpMediuInterogare = st.mean(timpiInterogare)
print("Timp mediu de interogare: %f" % timpMediuInterogare)



