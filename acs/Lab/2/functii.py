import numpy as np
from numpy import linalg as la
from matplotlib import pyplot as plt
import cv2
import math
import statistics

def train(cale, nrPersoane, nrPozeAntrenare, rezTotalaPoza):
  print("Adaugare poze in matricea de antrenare pentru %s persoane" % nrPersoane)

  nrTotalPoze = nrPersoane * nrPozeAntrenare
  A = np.zeros((nrTotalPoze, rezTotalaPoza))

  for i in range(1, nrPersoane + 1) :
    caleFolderPersoana = cale + "s" + str(i) + "/"
    for j in range(1, nrPozeAntrenare + 1) :
      calePozaAntrenare = caleFolderPersoana + str(j) + '.pgm'
      pozaAntrenare = np.array(cv2.imread(calePozaAntrenare, cv2.IMREAD_GRAYSCALE))
      pozaVect = pozaAntrenare.reshape(rezTotalaPoza,)
      indexPoza = (i-1) * nrPozeAntrenare + j
      A[indexPoza - 1] = pozaVect
  
  return A


def verificare_poza(A, persoana, poza, nrPozeAntrenare, rezPozaX, rezPozaY, afiseaza = 1):
  indexVerificare = nrPozeAntrenare * (persoana-1) + poza - 1
  #print("Verificare persoana %s poza %s, poza cu index %s in A" % (persoana, poza, indexVerificare))
  plt.imshow(A[indexVerificare].reshape(rezPozaX, rezPozaY), cmap='gray', vmin=0, vmax=255)

  if (afiseaza) :
    plt.show()

def poza_de_test(cale, persoana, poza, rezPozaX, rezPozaY, afiseaza = 1):
  #print("Verificare cu algoritm NN poza %s persoana %s" % (poza, persoana))

  calePozaCautata = cale + "s" + str(persoana) + "/"
  calePozaCautata += str(poza) + '.pgm'
  print("Poza de test" + calePozaCautata)

  rezTotalaPoza = rezPozaX * rezPozaY
  pozaCautata = np.array(cv2.imread(calePozaCautata, cv2.IMREAD_GRAYSCALE))
  pozaCautata = pozaCautata.reshape(rezTotalaPoza,)

  plt.imshow(pozaCautata.reshape(rezPozaX, rezPozaY), cmap='gray', vmin=0, vmax=255)

  if (afiseaza) :
    plt.show()

  return pozaCautata

def test_nn(A, nrTotalPoze, pozaCautata, nrPozeAntrenare, rezPozaX, rezPozaY, afiseaza = 1):
  norme = np.zeros(nrTotalPoze)

  for i in range(len(norme)) :
    norme[i] = la.norm(A[i] - pozaCautata)

  pozitia = np.argmin(norme)
  #print("S-a gasit imaginea de pe pozitia %s" % pozitia)
  nrPersoanaGasita = math.ceil(pozitia / nrPozeAntrenare);
  #print("Persoana: %s" % nrPersoanaGasita)

  if (afiseaza) :
    plt.imshow(A[pozitia].reshape(rezPozaX, rezPozaY), cmap='gray', vmin=0, vmax=255)
    plt.show()

  return nrPersoanaGasita

def test_knn(k, norma, A, nrTotalPoze, pozaCautata, nrPozeAntrenare, rezPozaX, rezPozaY, afiseaza = 1) :
  norme = np.zeros(nrTotalPoze)

  #la.norm(x,1)
  #la.norm(x,2) - norma euclidiana 
  #la.norm(x, np.inf)
  #1-np.dot(a[:,i],pozaCautata)/(la.norm(a[:,i])*la.norm(pozaCautata)) - norma cos
  for i in range(len(norme)) :
    if (norma == 'infinit') :
      norme[i] = la.norm(A[i] - pozaCautata, np.inf)
    elif (norma == 'cos') :
      norme[i] = 1-np.dot(A[i],pozaCautata)/(la.norm(A[i])*la.norm(pozaCautata))
    else :
      norme[i] = la.norm(A[i] - pozaCautata, norma)

  pozitii = np.argsort(norme)
  pozitii = pozitii[:k]
  pozitia = statistics.mode(pozitii)

  print("S-a gasit imaginea de pe pozitia %s" % pozitia)
  nrPersoanaGasita = math.ceil(pozitia / nrPozeAntrenare);
  print("Persoana: %s" % nrPersoanaGasita)

  if (afiseaza) :
    plt.imshow(A[pozitia].reshape(rezPozaX, rezPozaY), cmap='gray', vmin=0, vmax=255)
    plt.show()
