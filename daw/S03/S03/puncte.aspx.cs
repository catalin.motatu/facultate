﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace S03
{
    public partial class puncte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(Server.MapPath("App_Data/puncte.xml"));

            litInfo.Text = "Nr tabele: " + ds.Tables.Count.ToString() + "<br />";
            litInfo.Text += "Nr randuri: " + ds.Tables[0].Rows.Count.ToString() + "<br />";
            litInfo.Text += "Nr coloane: " + ds.Tables[0].Columns.Count.ToString() + "<br />";

            ds.Tables[0].Columns.Add(new DataColumn() { ColumnName = "coordonate", DataType = typeof(String) });
            foreach (DataRow linie in ds.Tables[0].Rows)
            {
                linie["coordonate"] = linie["x"].ToString() + ";" + linie["y"].ToString();
            }

            if (!Page.IsPostBack)
            {
                foreach (DataRow linie in ds.Tables[0].Rows)
                {
                    //ddlPuncte.Items.Add(new ListItem(linie["denumire"].ToString(), linie["x"].ToString()));
                    //ddlPuncte.Items.Add(new ListItem(linie["denumire"].ToString(), linie["coordonate"].ToString()));
                }

                // Data source list.
                ddlPuncte.DataSource = ds.Tables[0];
                ddlPuncte.DataTextField = "denumire";
                ddlPuncte.DataValueField = "coordonate";
                ddlPuncte.DataBind();

                // Data source radio button.
                rblPuncte.DataSource = ds.Tables[0];
                rblPuncte.DataTextField = "denumire";
                rblPuncte.DataValueField = "x";
                rblPuncte.DataBind();

                // Table generated in a loop.
                TableRow tr = new TableRow();
                foreach (DataColumn coloana in ds.Tables[0].Columns)
                {
                    TableCell td = new TableCell();
                    td.Text = coloana.ColumnName;
                    tr.Cells.Add(td);
                    tblPuncte.Rows.Add(tr);
                }

                foreach (DataRow linie in ds.Tables[0].Rows)
                {
                    TableRow rand = new TableRow();
                    foreach (DataColumn coloana in ds.Tables[0].Columns)
                    {
                        TableCell td = new TableCell();
                        td.Text = linie[coloana.ColumnName].ToString();
                        rand.Cells.Add(td);
                    }
                    tblPuncte.Rows.Add(rand);
                }


                // Data source grid view.
                gvPuncte.DataSource = ds.Tables[0];
                gvPuncte.DataBind();


                // Custom grid view.
                GridView gv = new GridView();
                gv.AutoGenerateColumns = false;
                gv.DataSource = ds;
                gv.Columns.Add(new BoundField() { HeaderText = "Denumire", DataField = "denumire" });
                gv.Columns.Add(new BoundField() { HeaderText = "Abscisa", DataField = "x" });
                gv.Columns.Add(new BoundField() { HeaderText = "Ordonata", DataField = "y" });
                gv.DataBind();
                phPuncte.Controls.Add(gv);

            }
       

        }
    }
}