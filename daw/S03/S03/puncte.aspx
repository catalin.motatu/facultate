﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="puncte.aspx.cs" Inherits="S03.puncte" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Literal ID="litInfo" runat="server"></asp:Literal>
        </div>
        <div>
            <asp:Label ID="lblRanduri" runat="server" Text="Puncte" Font-Bold="True"></asp:Label>
            <asp:DropDownList ID="ddlPuncte" runat="server"></asp:DropDownList>
            <br />

            <asp:Label ID="lblRanduriRadio" runat="server" Text="Puncte" Font-Bold="True"></asp:Label>
            <asp:RadioButtonList ID="rblPuncte" runat="server"></asp:RadioButtonList>

            <h2>Tabel foreach</h2>
            <div>
                <asp:Table ID="tblPuncte" runat="server" BorderWidth="1"></asp:Table>
            </div>

            <h2>GridView</h2>
            <div>
                <asp:GridView ID="gvPuncte" runat="server"></asp:GridView>
            </div>

            <h2>Custom GridView</h2>
            <div>
                <asp:PlaceHolder ID="phPuncte" runat="server"></asp:PlaceHolder>
            </div>
        </div>
    </form>
</body>
</html>
