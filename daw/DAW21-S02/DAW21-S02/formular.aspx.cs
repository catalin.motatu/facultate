﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

namespace DAW21_S02
{
    public partial class formular : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Fel principal.
                ddListFelPrincipal.Items.Add(new ListItem("Fel 1", "1"));
                ddListFelPrincipal.Items.Add(new ListItem("Fel 2", "2"));
                ddListFelPrincipal.Items.Add(new ListItem("Fel 3", "3"));
                ddListFelPrincipal.Items.Add(new ListItem("Fel 4", "4"));

                // Sos.
                radioListSos.Items.Add(new ListItem("Sos 1", "1"));
                radioListSos.Items.Add(new ListItem("Sos 2", "2"));
                radioListSos.Items.Add(new ListItem("Sos 3", "3"));
                radioListSos.Items.Add(new ListItem("Sos 4", "4"));

                // Supliment.
                cbkListSupliment.Items.Add(new ListItem("Supliment 1", "1"));
                cbkListSupliment.Items.Add(new ListItem("Supliment 2", "2"));
                cbkListSupliment.Items.Add(new ListItem("Supliment 3", "3"));
                cbkListSupliment.Items.Add(new ListItem("Supliment 4", "4"));
            } else
            {

            }
        }

        protected void Salveaza(object sender, EventArgs e)
        {

            if (reqFVPrenume.IsValid && reqFVNume.IsValid)
            {

                Dictionary<string, string> valori = new Dictionary<string, string>();
                valori["prenume"] = txtPrenume.Text;
                valori["nume"] = txtNume.Text;
                valori["adresa"] = txtAdresa.Text;

                if (ckbSalveazaAdresa.Checked)
                {
                    valori["salveaza"] = "DA";
                } else
                {
                    valori["salveaza_adresa"] = "NU";
                }

                valori["fel_principal"] = ddListFelPrincipal.SelectedItem.Text;

                if (radioListSos.SelectedItem != null) {
                    valori["sos"] = radioListSos.SelectedItem.Text;
                } else
                {
                    valori["sos"] = "Nespecificat";
                }
                
                StringBuilder sbSupliment = new StringBuilder();
                foreach (ListItem item in cbkListSupliment.Items)
                {
                    if (item.Selected)
                    {
                        sbSupliment.Append(item.Text).Append(",");
                    }
                    
                }
                valori["supliment"] = sbSupliment.ToString();

                foreach (KeyValuePair<string, string> entry in valori)
                {
                    TableCell label = new TableCell();
                    label.Text = entry.Key;
                    TableCell valoare = new TableCell();
                    valoare.Text = entry.Value;
                    TableRow tr = new TableRow();
                    tr.Cells.Add(label);
                    tr.Cells.Add(valoare);

                    tblComanda.Rows.Add(tr);
                }
            }
        }
    }
}