﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="formular.aspx.cs" Inherits="DAW21_S02.formular" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <div id="validari" style="color: red">

            </div>

            <div>
                <asp:Label ID="labelPrenume" runat="server" Text="Prenume" Font-Bold="true"></asp:Label>
                <br />
                <asp:TextBox ID="txtPrenume" runat="server"></asp:TextBox>
                <br />
                <div style="color:red">
                    <asp:RequiredFieldValidator
                        ID="reqFVPrenume" 
                        runat="server"
                        ErrorMessage="Prenumele este obligatoriu."
                        ControlToValidate="txtPrenume"
                        EnableClientScript="false"
                    ></asp:RequiredFieldValidator>
                </div>
            </div>

            <div>
                <asp:Label ID="labelNume" runat="server" Text="Nume" Font-Bold="true"></asp:Label>
                <br />
                <asp:TextBox ID="txtNume" runat="server"></asp:TextBox>
                <br />
                <div style="color:red">
                    <asp:RequiredFieldValidator
                        ID="reqFVNume" 
                        runat="server"
                        ErrorMessage="Numele este obligatoriu."
                        ControlToValidate="txtNume"
                        EnableClientScript="false"
                    ></asp:RequiredFieldValidator>
                </div>
            </div>

            <div>
                <asp:Label ID="labelAdresa" runat="server" Text="Adresa" Font-Bold="true"></asp:Label>
                <br />
                <asp:TextBox ID="txtAdresa" runat="server" TextMode="MultiLine"></asp:TextBox>
            </div>

            <div>
                <asp:CheckBox ID="ckbSalveazaAdresa" runat="server" Text="Salveaza adresa" />
            </div>

            <hr />

            <div>
                <asp:Label ID="labelFelPrincipal" runat="server" Text="Fel principal" Font-Bold="true"></asp:Label>
                <br />
                <asp:DropDownList ID="ddListFelPrincipal" runat="server"></asp:DropDownList>
            </div>

            <div>
                <asp:Label ID="labelSos" runat="server" Text="Sos" Font-Bold="true"></asp:Label>
                <br />
                <asp:RadioButtonList ID="radioListSos" runat="server"></asp:RadioButtonList>
            </div>

            <div>
                <asp:Label ID="labelSupliment" runat="server" Text="Supliment" Font-Bold="true"></asp:Label>
                <br />
                <asp:CheckBoxList ID="cbkListSupliment" runat="server"></asp:CheckBoxList>
            </div>
            <hr />

            <div>
                <asp:Button ID="salveaza" runat="server" Text="Salveaza" OnClick="Salveaza" />
            </div>

            <asp:Table ID="tblComanda" runat="server"></asp:Table>

        </div>
    </form>
</body>
</html>
