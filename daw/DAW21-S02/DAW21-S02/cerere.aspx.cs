﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DAW21_S02
{
    public partial class cerere : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            litTipCerere.Text = Request.HttpMethod;
            TableRow tr;
            TableCell td;
            // culegem antetele (Headers) și prăjiturelele (Cookies)
            tr = new TableRow();
            td = new TableCell();
            td.Text = "Headers";
            td.Font.Bold = true;
            td.Font.Size = FontUnit.Point(12);
            td.ColumnSpan = 2;
            tr.Cells.Add(td);
            tblDate.Rows.Add(tr);

            foreach(var cheie in Request.Headers.AllKeys)
            {
                tr = new TableRow();
                td = new TableCell();
                td.Text = cheie;
                td.Font.Bold = true;
                tr.Cells.Add(td);
                td = new TableCell();
                StringBuilder sb = new StringBuilder();
                foreach (var valoare in Request.Headers.GetValues(cheie))
                {
                    sb.Append("- " + valoare + "<br/>");
                }
                td.Text = sb.ToString();
                tr.Cells.Add(td);
                tblDate.Rows.Add(tr);
            }

            tr = new TableRow();
            td = new TableCell();
            td.Text = "Cookies";
            td.Font.Bold = true;
            td.Font.Size = FontUnit.Point(12);
            td.ColumnSpan = 2;
            tr.Cells.Add(td);
            tblDate.Rows.Add(tr);

        }

        protected void btnButon_Click(object sender, EventArgs e)
        {
            
        }
    }
}