package etaxi.modele;

/**
 *
 * @author catal
 */
public class Utilizator extends AModel {
    
    private String nume;
    private String parola;
    private int activ;
    
    public Utilizator(int id, String nume, String parola, int activ) {
        this.id = id;
        this.nume = nume;
        this.parola = parola;
        this.activ = activ;
    }
    
    public String getNume() {
        return this.nume;
    }
    
    public String getParola() {
        return this.parola;
    }
    
    public int getActiv() {
        return this.activ;
    }
    
    public void setNume(String nume) {
        this.nume = nume;
    }
    
    public void setParola(String parola) {
        this.parola = parola;
    }
    
    public void setActiv(int activ) {
        this.activ = activ;
    }
}
