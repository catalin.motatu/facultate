package etaxi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
    
    public static Connection con = null;
    private String url = "jdbc:sqlite:etaxi.db";
    
    private Connect() {
        try {
            // Creaza conexiunea.
            con = DriverManager.getConnection(url);
            System.out.println("Conexiune reusita la baza de date.");
        } catch (SQLException e) {
            // Afiseaza eroare SQL.
            System.err.println(e.getMessage());
            System.exit(0);
        }
    }
    
    public static Connection getConnection() {
        if (con == null) {
            System.out.println("Initializare conexiune baza date");
            new Connect();
        }
        
        return con;
    }
}
