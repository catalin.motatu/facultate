/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package etaxi;

import etaxi.modele.Utilizator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author catal
 */
public class UtilizatorManager {
    
    public Utilizator find(String nume, String parola) {
        Utilizator u = null;
        try {
            Connection con = Connect.getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM utilizator WHERE nume = ? AND parola = ?");
            stmt.setString(1, nume);
            stmt.setString(2, parola);
            ResultSet rs = stmt.executeQuery();
            
            u = new Utilizator(rs.getInt("id"), rs.getString("nume"), rs.getString("parola"), rs.getInt("activ"));
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        return u;
    }
    
    public Utilizator findById(int utilizatorId) {
        Utilizator u = null;
        try {
            Connection con = Connect.getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM utilizator WHERE id = ?");
            stmt.setInt(1, utilizatorId);
            ResultSet rs = stmt.executeQuery();
            
            u = new Utilizator(rs.getInt("id"), rs.getString("nume"), rs.getString("parola"), 1);
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        return u;
    }
    
    public int add(Utilizator u) {
        
        int utilizatorId = 0;
        
        try {
            Connection con = Connect.getConnection();
            PreparedStatement stmt = con.prepareStatement("INSERT INTO utilizator (nume, parola, activ) VALUES (?, ?, ?)");
            stmt.setString(1, u.getNume());
            stmt.setString(2, u.getParola());
            stmt.setInt(3, u.getActiv());
            stmt.executeUpdate();
            
            ResultSet rs = stmt.getGeneratedKeys();
            utilizatorId = rs.getInt(1);
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        return utilizatorId;
    }
    
    public Utilizator update(Utilizator u) {
        try {
            Connection con = Connect.getConnection();
            PreparedStatement stmt = con.prepareStatement("UPDATE utilizator SET nume = ?, parola = ?, activ = ? WHERE id = ?");
            stmt.setString(1, u.getNume());
            stmt.setString(2, u.getParola());
            stmt.setInt(3, u.getActiv());
            stmt.setInt(4, u.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        return u;
    }
}
