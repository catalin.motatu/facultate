/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package etaxi;

import etaxi.modele.Utilizator;

/**
 *
 * @author catal
 */
public class Auth {
    
    public static Utilizator utilizatorCurent = null;
    
    public static void initUtilizator(Utilizator utilizator) {
        utilizatorCurent = utilizator;
    }
    
    public static Utilizator getUtilizator() {
        return utilizatorCurent;
    }
}
