package etaxi.gui;

import etaxi.Auth;
import etaxi.UtilizatorManager;
import etaxi.modele.Utilizator;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author catal
 */
public class GuiComanda extends JFrame implements ActionListener {
    
    Utilizator utilizator;
    
    Container container = getContentPane();
    
    JLabel utilizatorLabel = new JLabel("");
    JButton btnDezactiveaza = new JButton("Dezactiveaza utilizator");
    JButton btnParola = new JButton("Schimba parola");
    JButton btnMasini = new JButton("Masini");
    JButton btnIesire = new JButton("Iesire");
    
    public GuiComanda() {
        
        initGui();
        
        utilizator = Auth.getUtilizator();
        utilizatorLabel.setText("Bine ai venit " + utilizator.getNume() + "!");
        
        container.setLayout(null);
        
        utilizatorLabel.setBounds(50, 10, 250, 30);
        btnDezactiveaza.setBounds(50, 50, 250, 30);
        btnParola.setBounds(50, 90, 250, 30);
        btnMasini.setBounds(50, 130, 250, 30);
        btnIesire.setBounds(50, 170, 250, 30);
        
        container.add(btnDezactiveaza);
        container.add(btnParola);
        container.add(btnMasini);
        container.add(btnIesire);
        container.add(utilizatorLabel);
        
        btnDezactiveaza.addActionListener(this);
        btnParola.addActionListener(this);
        btnMasini.addActionListener(this);
        btnIesire.addActionListener(this);
        
    }
    
    private void initGui() {
        this.setTitle("E-Taxi - Meniul principal");
        this.setBounds(100,100,350,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnDezactiveaza) {
            int input = JOptionPane.showConfirmDialog(null, "Confirmati dezactivarea utilizatorului?", "Dezactivare utilizator", JOptionPane.YES_NO_OPTION);
            if (input == 0) {
                utilizator.setActiv(0);
                try {
                    UtilizatorManager um = new UtilizatorManager();
                    um.update(utilizator);
                    JOptionPane.showMessageDialog(null, "Utilizatorul " + utilizator.getNume() + " a fost dezactivat. Revenire la autentificare.", "Dezactivare utilizator", JOptionPane.INFORMATION_MESSAGE);
                    
                    // Afiseaza autentificare.
                    GuiAutentificare autentificare = new GuiAutentificare();
                    autentificare.setVisible(true);
                    
                    // Inchide fereastra curenta.
                    this.setVisible(false);
                    this.dispose();
                    
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Nu s-a putut dezactiva utilizatorul", "Dezactivare utilizator", JOptionPane.ERROR_MESSAGE);
                    System.err.println(ex.getMessage());
                }
            }
        } else if (e.getSource() == btnParola) {
            GuiParola parola = new GuiParola();
            parola.setVisible(true);
        } else if (e.getSource() == btnMasini) {
            
        } else if (e.getSource() == btnIesire) {
            int input = JOptionPane.showConfirmDialog(null, "Doriti sa parasiti aplicatia?", "Iesire", JOptionPane.YES_NO_OPTION);
            if (input == 0) {
                System.exit(0);
            }
        }
    }
}
