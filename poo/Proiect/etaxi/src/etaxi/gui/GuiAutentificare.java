package etaxi.gui;

import etaxi.Auth;
import etaxi.UtilizatorManager;
import etaxi.modele.Utilizator;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Formular de autentificare.
 */
public class GuiAutentificare extends JFrame implements ActionListener {
    
    Container container = getContentPane();
    
    JLabel eroareLabel = new JLabel();
    JLabel utilizatorLabel = new JLabel("Nume utilizator");
    JLabel parolaLabel = new JLabel("Parola");
    JTextField utilizatorText = new JTextField();
    JPasswordField parolaText = new JPasswordField();
    JButton btnAutentificare = new JButton("Autentificare");
    JButton btnUtilizatorNou = new JButton("Utilizator nou");
    
    
    public GuiAutentificare() {
        
        initGui();
        
        container.setLayout(null);
        
        // Dimensiuni.
        eroareLabel.setBounds(50,10, 300, 30);
        eroareLabel.setForeground(Color.red);
        
        utilizatorLabel.setBounds(50, 50, 100,30);
        utilizatorText.setBounds(150, 50, 200, 30);
        
        parolaLabel.setBounds(50,90,100,30);
        parolaText.setBounds(150,90, 200, 30);
        
        btnAutentificare.setBounds(50,130,150,30);
        btnUtilizatorNou.setBounds(200,130,150,30);
        
        // Adauga in container.
        container.add(eroareLabel);
        container.add(utilizatorLabel);
        container.add(parolaLabel);
        container.add(utilizatorText);
        container.add(parolaText);
        container.add(btnAutentificare);
        container.add(btnUtilizatorNou);
        
        // Defineste evenimente.
        btnAutentificare.addActionListener(this);
        btnUtilizatorNou.addActionListener(this);
    }
    
    private void initGui() {
        // Initializeaza interfata grafica.
        this.setTitle("E-Taxi - Autentificare");
        this.setBounds(100,100,410,250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnAutentificare) {
            
            // Goleste eticheta de eroare.
            eroareLabel.setText("");
            
            try {
                // Valideaza formularul.
                validateInput();
                
                // Initializeaza managerul de utilizatori.
                UtilizatorManager um = new UtilizatorManager();
            
                // Cauta utilizator dupa nume si parola.
                Utilizator u = um.find(utilizatorText.getText(), new String(parolaText.getPassword()));
                
                if (u != null) {
                    if (u.getActiv() > 0) {
                        System.out.println("Utilizator autentificat!");
                    
                        // Stocheaza utilizatorul in clasa de autentificare.
                        Auth.initUtilizator(u);

                        // Afiseaza panoul de comanda.
                        GuiComanda comanda = new GuiComanda();
                        comanda.setVisible(true);
                        
                        // Ascunde panou autentificare.
                        this.setVisible(false);
                    } else {
                        eroareLabel.setText("Utilizatorul este inactiv.");
                    }
                } else {
                    eroareLabel.setText("Nu a fost gasit utilizatorul.");
                }
            } catch (Exception ex) {
                eroareLabel.setText(ex.getMessage());
                System.err.println(ex.getMessage());
            }
        } else if (e.getSource() == btnUtilizatorNou) {
            // Afiseaza interfata de adaugare utilizator.
            GuiInregistrare inregistrare = new GuiInregistrare();
            inregistrare.setVisible(true);
            
            // Ascunde panou autentificare.
            this.setVisible(false);
        }
    }
    
    private void validateInput() throws Exception {
        
        // Valideaza nume utilizator.
        if (utilizatorText.getText().isEmpty() == true) {
            throw new Exception("Numele utilizatorului trebuie specificat.");
        }
        
        // Valideaza parola.
        if (new String(parolaText.getPassword()).isEmpty() == true) {
            throw new Exception("Parola trebuie specificata.");
        }
    }
}
