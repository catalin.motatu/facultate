package etaxi.gui;

import etaxi.Auth;
import etaxi.UtilizatorManager;
import etaxi.modele.Utilizator;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Formular de schimbare parola.
 */
public class GuiParola extends JFrame implements ActionListener {
    
    Container container = getContentPane();
    
    JLabel eroareLabel = new JLabel();
    JLabel lblParolaCurenta = new JLabel("Parola curenta");
    JLabel lblParolaNoua = new JLabel("Parola noua");
    JPasswordField txtParolaCurenta = new JPasswordField();
    JPasswordField txtParolaNoua = new JPasswordField();
    
    JButton btnSchimbaParola = new JButton("Schimba parola");
    
    public GuiParola() {
        
        initGui();
        
        container.setLayout(null);
        
        // Eticheta de eroare.
        eroareLabel.setBounds(50,10, 300, 30);
        eroareLabel.setForeground(Color.red);
        
        // Dimensiuni.
        lblParolaCurenta.setBounds(50, 50, 100, 30);
        txtParolaCurenta.setBounds(150, 50, 200, 30);
        
        lblParolaNoua.setBounds(50,90,100,30);
        txtParolaNoua.setBounds(150,90, 200, 30);
        
        btnSchimbaParola.setBounds(200,130,150,30);
        
        // Adauga in container.
        container.add(eroareLabel);
        container.add(lblParolaCurenta);
        container.add(lblParolaNoua);
        container.add(txtParolaCurenta);
        container.add(txtParolaNoua);
        container.add(btnSchimbaParola);
        
        // Defineste evenimente.
        btnSchimbaParola.addActionListener(this);
    }
    
    private void initGui() {
        this.setTitle("E-Taxi - Schimba parola");
        this.setBounds(100,100,410,250);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnSchimbaParola) {
            try {
                Utilizator utilizator = Auth.getUtilizator();
                
                validateInput(utilizator);
                
                utilizator.setParola(new String(txtParolaNoua.getPassword()));
                
                UtilizatorManager um = new UtilizatorManager();
                
                um.update(utilizator);
                
                JOptionPane.showMessageDialog(null, "Parola a fost modificata. Revenire la autentificare.", "Schimba parola", JOptionPane.INFORMATION_MESSAGE);
                    
                // Afiseaza autentificare.
                GuiAutentificare autentificare = new GuiAutentificare();
                autentificare.setVisible(true);

                // Inchide fereastra curenta.
                this.setVisible(false);
                this.dispose();
                
            } catch (Exception ex) {
                eroareLabel.setText(ex.getMessage());
                System.err.println(ex.getMessage());
            }
        }
    }
    
    public void validateInput(Utilizator u) throws Exception {
        if (new String(txtParolaCurenta.getPassword()).isEmpty() == true) {
            throw new Exception("Specificati parola curenta.");
        }
        
        if (!new String(txtParolaCurenta.getPassword()).equals(u.getParola())) {
            throw new Exception("Parola curenta este incorecta.");
        }
        
        if (new String(txtParolaNoua.getPassword()).isEmpty() == true) {
            throw new Exception("Specificati parola noua.");
        }
        
        
        
    }
}
