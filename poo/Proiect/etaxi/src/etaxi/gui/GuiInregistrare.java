package etaxi.gui;

import etaxi.Auth;
import etaxi.UtilizatorManager;
import etaxi.modele.Utilizator;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Formular de inregistrare.
 */
public class GuiInregistrare extends JFrame implements ActionListener {
    
    Container container = getContentPane();
    
    JLabel eroareLabel = new JLabel();
    JLabel utilizatorLabel = new JLabel("Nume utilizator");
    JLabel parolaLabel = new JLabel("Parola");
    JTextField utilizatorText = new JTextField();
    JPasswordField parolaText = new JPasswordField();
    JButton btnInregistrare = new JButton("Inregistrare");
    
    
    public GuiInregistrare() {
        
        initGui();
        
        container.setLayout(null);
        
        // Dimensiuni.
        eroareLabel.setBounds(50,10, 300, 30);
        eroareLabel.setForeground(Color.red);
        
        utilizatorLabel.setBounds(50, 50, 100, 30);
        utilizatorText.setBounds(150, 50, 200, 30);
        
        parolaLabel.setBounds(50,90,100,30);
        parolaText.setBounds(150,90, 200, 30);
        
        btnInregistrare.setBounds(200,130,150,30);
        
        // Adauga in container.
        container.add(eroareLabel);
        container.add(utilizatorLabel);
        container.add(parolaLabel);
        container.add(utilizatorText);
        container.add(parolaText);
        container.add(btnInregistrare);
        
        // Defineste evenimente.
        btnInregistrare.addActionListener(this);
    }
    
    private void initGui() {
        this.setTitle("E-Taxi - Inregistrare");
        this.setBounds(100,100,410,250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnInregistrare) {
            try {
                validateInput();
                
                Utilizator u = new Utilizator(0, utilizatorText.getText(), new String(parolaText.getPassword()), 1);
                UtilizatorManager um = new UtilizatorManager();
                
                int utilizatorId = um.add(u);
                
                if (utilizatorId > 0) {
                    Utilizator utilizatorNou = um.findById(utilizatorId);
                    if (utilizatorNou != null) {
                        Auth.initUtilizator(utilizatorNou);
                        
                        // Afiseaza panoul de comanda.
                        GuiComanda comanda = new GuiComanda();
                        comanda.setVisible(true);
                        this.setVisible(false);
                    }
                } else {
                    eroareLabel.setText("Nu s-a putut adauga utilizatorul.");
                }
            } catch (Exception ex) {
                eroareLabel.setText(ex.getMessage());
                System.err.println(ex.getMessage());
            }
        }
    }
    
    private void validateInput() throws Exception {
        if (utilizatorText.getText().isEmpty() == true) {
            throw new Exception("Numele utilizatorului trebuie specificat.");
        }
        if (new String(parolaText.getPassword()).isEmpty() == true) {
            throw new Exception("Parola trebuie specificata.");
        }
    }
}
