
package ebiblioteca.view;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;

/**
 *
 * @author catal
 */
public class FereastraReturnare extends JFrame implements ActionListener {
    
    Container container = getContentPane();
    
    JLabel lblNumePrenume = new JLabel("Nume si prenume:");
    JLabel lblCnp = new JLabel("Cod de identificare CNP:");
    JLabel lblCartiImprumutate = new JLabel("Carti imprumutate:");
    JLabel lblPlata = new JLabel("Plata:");
    
    JTextField txtNumePrenume = new JTextField();
    JTextField txtCnp = new JTextField();
    JTextField txtPlata = new JTextField();
    JList lstCartiImprumutate = new JList();
    JButton btnReturnare = new JButton("OK");
    
    public FereastraReturnare() {
        
        initGui();
        
        // Initializare layout gol.
        container.setLayout(null);
        
        // Pozitionare campuri.
        lblNumePrenume.setBounds(10, 10, 150, 30);
        lblCnp.setBounds(10, 50, 150, 30);
        lblCartiImprumutate.setBounds(10, 90, 150, 30);
        lblPlata.setBounds(10, 180, 150, 30);
        
        txtNumePrenume.setBounds(170, 10, 250, 30);
        txtCnp.setBounds(170, 50, 250, 30);
        lstCartiImprumutate.setBounds(170, 90, 250, 30);
        txtPlata.setBounds(170, 130, 250, 30);
        
        // Dezactiveaza campuri.
        txtNumePrenume.setEnabled(false);
        txtCnp.setEnabled(false);
        lstCartiImprumutate.setEnabled(false);
        txtPlata.setEnabled(false);
        
        // Pozitionare butoane.
        btnReturnare.setBounds(170, 280, 150, 30);
        
        // Adauga in container.
        container.add(lblNumePrenume);
        container.add(lblCnp);
        container.add(lblCartiImprumutate);
        container.add(lblPlata);
        container.add(txtNumePrenume);
        container.add(txtCnp);
        container.add(lstCartiImprumutate);
        container.add(txtPlata);
        container.add(btnReturnare);
        
        initEvents();
    }
    
    private void initGui() {
        // Initializeaza interfata grafica.
        this.setTitle("Returnare");
        this.setBounds(100,100,500,370);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }
    
    private void initEvents() {
        // Defineste evenimente.
        btnReturnare.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnReturnare) {
            try {
                
            } catch (Exception ex) {
                
            }
        }
    }
}
