
package ebiblioteca.view;

import ebiblioteca.model.Carte;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;

/**
 *
 * @author catal
 */
public class FereastraImprumut extends JFrame implements ActionListener {

    private ArrayList<Carte> carti;
    
    Container container = getContentPane();
    
    JLabel lblNumePrenume = new JLabel("Nume si prenume:");
    JLabel lblCnp = new JLabel("Cod de identificare CNP:");
    JLabel lblCarti = new JLabel("Lista carti:");
    JLabel lblCartiSelectate = new JLabel("Lista carti selectate:");
    JLabel lblDataReturnare = new JLabel("Data returnare:");
    
    JTextField txtNumePrenume = new JTextField();
    JTextField txtCnp = new JTextField();
    JList lstCarti = new JList();
    JList lstCartiSelectate = new JList();
    JTextField txtData = new JTextField();
    
    JButton btnAdaugaCarte = new JButton("Adauge carte");
    JButton btnImprumuta = new JButton("Imprumuta");
    
    public FereastraImprumut(ArrayList<Carte> carti) {
        
        this.carti = carti;
        
        initGui();
        
        // Initializare layout gol.
        container.setLayout(null);
        
        // Pozitionare campuri.
        lblNumePrenume.setBounds(10, 10, 150, 30);
        lblCnp.setBounds(10, 50, 150, 30);
        lblCarti.setBounds(10, 90, 150, 30);
        lblCartiSelectate.setBounds(10, 130, 150, 30);
        lblDataReturnare.setBounds(10, 230, 150, 30);
        
        txtNumePrenume.setBounds(170, 10, 250, 30);
        txtCnp.setBounds(170, 50, 250, 30);
        lstCarti.setBounds(170, 90, 250, 30);
        lstCartiSelectate.setBounds(170, 130, 250, 30);
        txtData.setBounds(170, 230, 250, 30);
        
        // Pozitionare butoane.
        btnImprumuta.setBounds(170, 280, 150, 30);
        
        // Adauga in container.
        container.add(btnImprumuta);
        container.add(lblNumePrenume);
        container.add(lblCnp);
        container.add(lblCarti);
        container.add(lblCartiSelectate);
        container.add(lblDataReturnare);
        container.add(txtNumePrenume);
        container.add(txtCnp);
        container.add(lstCarti);
        container.add(lstCartiSelectate);
        container.add(txtData);
        
        initEvents();
    }
    
    private void initGui() {
        // Initializeaza interfata grafica.
        this.setTitle("Imprumut");
        this.setBounds(100,100,500,370);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }
    
    private void initEvents() {
        // Defineste evenimente.
        btnImprumuta.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnImprumuta) {
            try {
                validare();
            } catch (Exception ex) {
                
            }
        }
    }
    
    private void validare() throws Exception {
        
    }
}
