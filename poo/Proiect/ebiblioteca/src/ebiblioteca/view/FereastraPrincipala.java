
package ebiblioteca.view;

import ebiblioteca.model.Carte;
import ebiblioteca.model.Imprumut;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author catal
 */
public class FereastraPrincipala extends JFrame implements ActionListener {
    
    private ArrayList<Carte> carti;
    private ArrayList<Imprumut> imprumuturi;
    
    Container container = getContentPane();    
    JButton btnImprumut = new JButton("Imprumut");
    JButton btnInapoiere = new JButton("Inapoiere");
    
    public FereastraPrincipala(ArrayList<Carte> carti, ArrayList<Imprumut> imprumuturi) {
        
        this.carti = carti;
        this.imprumuturi = imprumuturi;
        
        initGui();
        
        container.setLayout(null);
        
        // Pozitionare butoane.
        btnImprumut.setBounds(20, 20, 150, 30);
        btnInapoiere.setBounds(180, 20, 150, 30);
        
        // Adauga in container.
        container.add(btnImprumut);
        container.add(btnInapoiere);
        
        initEvents();
    }
    
    private void initGui() {
        // Initializeaza interfata grafica.
        this.setTitle("E-Biblioteca");
        this.setBounds(100,100,360,100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }
    
    private void initEvents() {
        // Defineste evenimente.
        btnImprumut.addActionListener(this);
        btnInapoiere.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnImprumut) {
            FereastraImprumut imprumut = new FereastraImprumut(carti);
            imprumut.setVisible(true);
        } else if (e.getSource() == btnInapoiere) {
            FereastraCautare cautare = new FereastraCautare();
            cautare.setVisible(true);
        }
    }
}
