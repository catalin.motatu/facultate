
package ebiblioteca.view;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author catal
 */
public class FereastraCautare extends JFrame implements ActionListener {
    
    Container container = getContentPane();
    
    JLabel lblCnp = new JLabel("CNP");
    JTextField txtCnp = new JTextField();
    JButton btnCauta = new JButton("Cauta");
    
    public FereastraCautare() {
        
        initGui();
        
        container.setLayout(null);
        
        // Pozitionare butoane.
        lblCnp.setBounds(20, 20, 30, 30);
        txtCnp.setBounds(60, 20, 140, 30);
        btnCauta.setBounds(220, 20, 100, 30);
        
        // Adauga in container.
        container.add(lblCnp);
        container.add(txtCnp);
        container.add(btnCauta);
        
        initEvents();
    }
    
    private void initGui() {
        // Initializeaza interfata grafica.
        this.setTitle("Cautare");
        this.setBounds(100,100,360,100);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }
    
    private void initEvents() {
        // Defineste evenimente.
        btnCauta.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnCauta) {
            try {
                validare();
                
                // Daca inputul este valid se efectueaza cautarea.
                FereastraReturnare returnare = new FereastraReturnare();
                returnare.setVisible(true);
                
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Eroare", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    private void validare() throws Exception {
        if (txtCnp.getText().isEmpty()) {
            throw new Exception("CNP-ul trebuie specificat.");
        }
    }
}
