/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ebiblioteca;

import ebiblioteca.model.Carte;
import ebiblioteca.model.Imprumut;
import ebiblioteca.view.FereastraPrincipala;
import java.util.ArrayList;


public class Aplicatie {
    
    private ArrayList<Carte> carti;
    private ArrayList<Imprumut> imprumuturi;
    
    // Manager stocare date.
    private ManagerDate md = new ManagerDate();
    
    public void start() {
        
        carti = md.getListaCarti();
        imprumuturi = md.getListaImprumuturi();
        
        FereastraPrincipala fereastra = new FereastraPrincipala(carti, imprumuturi);
        fereastra.setVisible(true);
    }
}
