
package ebiblioteca;

import ebiblioteca.model.Carte;
import ebiblioteca.model.Imprumut;
import ebiblioteca.view.FereastraPrincipala;
import java.util.ArrayList;

/**
 *
 * @author catal
 */
public class Ebiblioteca {
    
    public static void main(String[] args) {
        
        Aplicatie app = new Aplicatie();
        app.start();
        
    }
    
}
