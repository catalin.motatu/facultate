
package ebiblioteca;

import ebiblioteca.model.Carte;
import ebiblioteca.model.Imprumut;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;


public class ManagerDate {
    
    final String separator = "_";
    final String fisierCarti = "src\\data\\carti.txt";
    final String fisierImprumuturi = "src\\data\\imprumuturi.txt";
    
    ManagerDate() {
        
    }
    
    public ArrayList<Carte> getListaCarti() {
        
        ArrayList<Carte> carti = new ArrayList<Carte>();
        
        File fisier = new File(System.getProperty("user.dir") + "\\" + this.fisierCarti);
        Scanner inputStream;
        
        try{
            inputStream = new Scanner(fisier);

            while(inputStream.hasNext()){
                String line= inputStream.next();
                String[] valori = line.split(this.separator);
                
                Carte c = new Carte();
                c.setCodISBN(valori[0]);
                c.setTtitlu(valori[1]);
                c.setNumeEditura(valori[2]);
                c.setImprumutata(Boolean.parseBoolean(valori[3]));
                
                carti.add(c);
            }
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        return carti;
    }
    
    public ArrayList<Imprumut> getListaImprumuturi() {
        return null;
    }
    
    public Boolean salveazaImprumuturi(ArrayList<Imprumut> imprumuturi) {
        return false;
    }
    
    
    
}
