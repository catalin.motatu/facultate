
package ebiblioteca.model;

import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author catal
 */
public class Imprumut {
    
    private String nume;
    private String cnp;
    private Calendar dataReturnare;
    private ArrayList<Carte> carti;
    
    public Imprumut() {
        
    }
    
    public String getNume() {
        return nume;
    }
    
    public String getCnp() {
        return cnp;
    }
    
    public Calendar getDataReturnare() {
        return dataReturnare;
    }
    
    public ArrayList<Carte> getCarti() {
        return carti;
    }
    
    public void setNume(String nume) {
        this.nume = nume;
    }
    
    public void setCnp(String cnp) {
        this.cnp = cnp;
    }
    
    public void setDataReturnare(Calendar data) {
        this.dataReturnare = data;
    }
    
    public void setCarti(ArrayList<Carte> carti) {
        this.carti = carti;
    }
    
    public void addCarte(Carte c) {
        this.carti.add(c);
    }
    
}
