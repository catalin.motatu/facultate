
package ebiblioteca.model;

public class Carte {
    private String codISBN;
    private String titlu;
    private String numeEditura;
    private Boolean imprumutata;
    
    public Carte() {
        
    }
    
    public String getCodISBN() {
        return codISBN;
    }
    
    public String getTtitlu() {
        return titlu;
    }
    
    public String getNumeEditura() {
        return numeEditura;
    }
    
    public Boolean getImprumutata() {
        return imprumutata;
    }
    
    public void setCodISBN(String cod) {
        codISBN = cod;
    }
    
    public void setTtitlu(String titlu) {
        this.titlu = titlu;
    }
    
    public void setNumeEditura(String numeEditura) {
        this.numeEditura = numeEditura;
    }
    
    public void setImprumutata(Boolean imprumutata) {
        this.imprumutata = imprumutata;
    }
}
