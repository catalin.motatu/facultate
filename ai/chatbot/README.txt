## Despre
Chatbot.py este o implemetare a unui chatbot simplu cu ajutorul realizata cu ajutorul
librariei python chatterbot.
Poate identifica 3 utilizatori diferiti in functie de numele introdus ca raspuns la prima intrebare. Numele recunoscute sunt: Catalin, Ioana si Alex. Boot-ul va purta conversatii personalizate in functie de numele introduse astfle: Cu Catalin va purta o conversatie despre calatorii recente, cu Ioana despre filme preferate iar cu Alex o simpla conversatie.

## Tehnologii necesare
Pentru a rula scriptul este necesar sa aveti instalat **Python**, cel putin versiunea 2.x
Pentru instalarea libariilor este necesar utilizatul **pip**

## Instalare
```ssh
pip install chatterbot
```

## Executie aplicatie
```ssh
python chatbot.py
```

## Resurse folosite
Documentatie chatterbot: https://chatterbot.readthedocs.io/en/stable/index.html

## Posibile imbunatatiri
- Imbunatatirea setului de conversatii.
- Utilizarea trainerului **ChatterBot Corpus** pentru extinderea bazei de cunostinte.


## Previzualizare

![](./view.png "").