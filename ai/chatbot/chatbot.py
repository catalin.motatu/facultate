from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
#from chatterbot.trainers import ChatterBotCorpusTrainer

bot = ChatBot(
    name = "AIBot",
    read_only = True,
    database_uri=None,
    logic_adapters = ["chatterbot.logic.BestMatch", "chatterbot.logic.MathematicalEvaluation"]
)

#corpus_trainer = ChatterBotCorpusTrainer(bot)
#corpus_trainer.train('chatterbot.corpus.english')

labels_catalin = [
    'hi',
    'Hi there, where did you travel this year?',
    'Italy',
    'Hmm... I love Italy. Any place else?',
    'Romania is nice, I have been there once',
    'Good to hear'
]
labels_ioana = [
    'hi',
    'Hi there, What movies do you like?',
    'action movies',
    'Nice, I like action movies too!',
    'Cool',
    'Do you like comedies?',
    'Yes, do you?',
    'I do too!'
]
labels_alex = [
    'hi',
    'hi there',
    'how are you',
    'I\'m fine, and you?',
    'I\m ok',
    'Good to hear...',
    'Alright!',
    'bye',
    'See you!',
]
labels_unknown = [
    'Hi, I don\'t know you. What is your name again?',
    'What is your name again?',
    'Okk..... bye'
]

print (f"{bot.name}: Hi there! What is your name?")

labels = labels_unknown
name = input("You: ")
tag = 'unknown'

if (name.lower() == 'catalin') :
    tag = name.lower()
    labels = labels_catalin
elif (name.lower() == 'ioana') :
    tag = name.lower()
    labels = labels_ioana
elif (name.lower() == 'alex') :
    tag = name.lower()
    labels = labels_alex

trainer = ListTrainer(bot)
trainer.train(labels)

if (tag != 'unknown') :
    print (f"{bot.name}: Welcome back {name.capitalize()}, Hello!")
else :
    print (f"{bot.name}: Nice to meet you {name.capitalize()}. I don't know you.")

while True:
    try:
        bot_input = input("You: ")
        bot_response = bot.get_response(bot_input)
        print (f"{bot.name}: {bot_response}")
    except (KeyboardInterrupt, EOFError, SystemExit):
        break;