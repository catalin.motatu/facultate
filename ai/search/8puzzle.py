import numpy as np
import time
import copy
import sys

vizitat = []
goal = [1,2,3,4,5,6,7,8,0]

def gasesteStart (puzzle) :
    for i in range(9):
        if puzzle[i] == 0 :
            return i
        
def afiseazaStare(puzzle) :
    print("")
    for i in range(9) :
        if i % 3 == 0 : 
            print ("|" + str(puzzle[i]), end="")
        elif (i+1) % 3 == 0 :
            print (str(puzzle[i]) + "|", end="\n")
        else :
            print ("|" + str(puzzle[i]) + "|", end="")

def stareFinala(puzzle) :
    global goal
    return np.array_equal(puzzle, goal)

def miscaInSus(puzzle) :
    i = gasesteStart(puzzle)
    newPuzzle = copy.deepcopy(puzzle)
    if (i > 2) :
        newPuzzle[i] = newPuzzle[i-3]
        newPuzzle[i-3] = 0
        return newPuzzle
    else :
        return None

def miscaInJos(puzzle) :
    i = gasesteStart(puzzle)
    newPuzzle = copy.deepcopy(puzzle)
    if (i < 6) :
        newPuzzle[i] = newPuzzle[i+3]
        newPuzzle[i+3] = 0
        return newPuzzle
    else:
        return None

def miscaInStanga(puzzle) :
    i = gasesteStart(puzzle)
    newPuzzle = copy.deepcopy(puzzle)
    if i % 3 != 0 :
        newPuzzle[i] = newPuzzle[i-1]
        newPuzzle[i-1] = 0
        return newPuzzle
    else :
        return None

def miscaInDreapta(puzzle) :
    i = gasesteStart(puzzle)
    newPuzzle = copy.deepcopy(puzzle)
    if (i + 1) % 3 != 0 :
        newPuzzle[i] = newPuzzle[i+1]
        newPuzzle[i+1] = 0
        return newPuzzle
    else :
        return None

def dejaVizitat(node) :
    for n in vizitat :
        if  np.array_equal(n, node) :
            return True

    return False

def cautaVecini(nod) :

    vecini = []

    vecin = miscaInSus(nod)
    if (vecin and dejaVizitat(vecin) == False) :
        vizitat.append(vecin)
        vecini.append(vecin)

    vecin = miscaInJos(nod)
    if (vecin and dejaVizitat(vecin) == False) :
        vizitat.append(vecin)
        vecini.append(vecin)

    vecin = miscaInStanga(nod)
    if (vecin and dejaVizitat(vecin) == False) :
        vizitat.append(vecin)
        vecini.append(vecin)

    vecin = miscaInDreapta(nod)
    if (vecin and dejaVizitat(vecin) == False) :
        vizitat.append(vecin)
        vecini.append(vecin)
    
    return vecini


def bfs(puzzle) :

    queue = []
    cost = 0

    vizitat.append(puzzle)
    queue.append(puzzle)

    while queue :
        s = queue.pop(0)

        cost += 1
        print ("Step: %s " % cost)

        if stareFinala(s) == True :
            return s, cost

        queue.extend(cautaVecini(s))

        
def greedy(puzzle) :
    queue = []
    cost = 0

    vizitat.append(puzzle)
    queue.append(puzzle)

    while queue :
        print ("")
        print ("Citeste urmatoarea stare din stiva")
        current = queue.pop(0)
        afiseazaStare(current)
        
        cost += 1
        # Verifica daca elementul curent este solutia finala.
        if stareFinala(current) == True :
            return current, cost

        # Extinde stiva cu noi noduri.
        queue.extend(cautaVecini(current))
        # Sorteaza stiva extinsa
        queue = sortQueue(queue)

class n:
    def __init__(self, state, scor):
        self.state = state
        self.score = scor
        
def sortQueue(queue) :
    finalSortedQueue = []
    sortedQueue = []
    for item in queue :

        dmatch = h(item)
        sortedQueue.append(n(item, dmatch))
        
    sortedQueue.sort(key = lambda x: x.score)
    for i in sortedQueue :
        finalSortedQueue.append(i.state)

    return finalSortedQueue

def h(state):
    global goal

    dmatch = 0
    for i in range(0,9):
        if state[i] != goal[i]:
            dmatch +=1
    return dmatch

#===================================

puzzle_list = [
    [1,2,3,4,5,6,7,0,8],
    [1,2,3,4,5,0,6,7,8],
    [0,4,3,2,1,6,8,7,5]
]

print("Algoritmi de sortare - problema 8 puzzle")
print("Alege puzzle:")
print("1) Usor")
print("2) Mediu")
print("3) Greu")
print("4) Custom")
ales = input()
if ales == "1" or ales == "2" or ales == "3" :
    puzzle = puzzle_list[int(ales) - 1]
else :
    print("Definiti structura puzzle custom (ex. 1 2 3 5 4 6 0 8):")
    puzzle = [int(i) for i in input().split()]

if len(puzzle) != 9 :
    print ("Puzzle invalid")
    sys.exit()

print("Alege algoritm de rezolvare:")
print("1) Breadth First Search")
print("2) Greedy Best First Search")
nrAlg = input()

numeAlgoritm = "Breadth First Search"
alg = "bfs"
if nrAlg != "1" :
    numeAlgoritm = "Greedy Best First Search"
    alg = "greedy"

print("Puzzle ales:")
afiseazaStare(puzzle)
print("Se va rezolva cu algoritmul %s" % numeAlgoritm)
input("Apasati Enter pentru a incepe")

stari_testate = 0
timp_start = time.time()

if alg == "bfs" :
    solutie, cost = bfs(puzzle)
else :
    solutie, cost = greedy(puzzle)

print ("")
print ("===PUZZLE START===", end="\n")
afiseazaStare(puzzle)
print ("\n===SOLUTIE===", end="\n")
afiseazaStare(solutie)
print ("\nAlgoritm folosit: %s " % numeAlgoritm)
print ("Stari vizitate: %s" % cost)
print("Timp executie %s secunde" % (time.time() - timp_start))
print("")


    


        

