## Despre
8puzzle.py implementeaza o solutie pentru problema 8-puzzle folosind doi algoritmi diferiti: Breadth First Search si Greedy Best First Search.
Primeste ca date de intrare, puzzelul, care poate fi ales dintr-o lista sau specificat de la tastatura si algoritmul utilizat.
![](./view_1.png "").
Dupa apasarea tastei Enter, problema aleasa va fi rezolvata cu algoritmul selectat si rezultatul final va fi afisat in consola.

## Tehnologii necesare
Pentru a rula scriptul este necesar sa aveti instalat **Python**, cel putin versiunea 2.x
Pentru instalarea libariilor este necesar utilizatul **pip**

## Instalare
```ssh
pip install numpy
```

## Executie aplicatie
```ssh
python 8puzzle.py
```

## Rezultate optinute
### Breadth First Search

**Puzzle usor:**
![](./view_2.png "").

**Puzzle mediu**

![](./view_3.png "").

### Greedy Best First Search

**Puzzle usor:**

![](./view_4.png "").

**Puzzle mediu:**

![](./view_5.png "").

**Puzzle greu:**

![](./view_6.png "").